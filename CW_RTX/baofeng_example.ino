void setup() {
  pinMode(3, OUTPUT);
}

void loop() {
  /**
   * porteuse de 3 secondes toutes les 5min
   */
  digitalWrite(3, LOW); //PTT Init/release
  delay(300000);
  digitalWrite(3, HIGH); //PTT enable
  delay(3000); 
}
