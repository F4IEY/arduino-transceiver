/* SIMPLE MORSE BEACON
 *  V1.0
 *  PIN SORTIE DE SIGNAL: outPin
 */
#define WPM 20
int outPin = 3;
int vfo = 17500;
//WPM conversion
unsigned long wpmToMillis(int wpm) {
  //le mot de référence est PARIS: 50 éléments
  unsigned long ptMs = 60000/(wpm*50);
  return ptMs; 
}
unsigned long tdot = wpmToMillis(WPM);
unsigned long tdash = 3*tdot;
void setup() {
  pinMode(outPin, OUTPUT);
  Serial.begin(9600);
  sendMsg("CQCQ DE F4IEY F4IEY BEACON 73 VA");
}

void loop() { 
  if(Serial.available()){
    sendMsg(Serial.readString()); 
  }  
  delay(100);    
  }

void dot() {
//on écrit un point
    tone(outPin, vfo);
    delay(tdot);
    noTone(outPin);
    delay(tdot); //espace inter-charactère
}
void dash(){
  //on écrit un trait
    tone(outPin, vfo);
    delay(tdash);
    noTone(outPin);
    delay(tdot); //espace inter-charactère
}  
void sendMsg(String str)
{
  Serial.print("\nTX: ");
  for(int i=0;i<str.length();i++)
  {
    switch (str.charAt(i))
    {
    case 'A':
      dot();dash();break;
    case 'B':
      dash();dot();dot();dot();break;
    case 'C':
      dash();dot();dash();dot();break;
    case 'D':
      dash();dot();dot();break;
    case 'E':
      dot();break;
    case 'F':
      dot();dot();dash();dot();break;
    case 'G':
      dash();dash();dot();break;
    case 'H':
      dot();dot();dot();dot();break;
    case 'I':
      dot();dot();break;
    case 'J':
      dot();dash();dash();dash();break;
    case 'K':
      dash();dot();dash();break;
    case 'L':
      dot();dash();dot();dot();break;
    case 'M':
      dash();dash();break;
    case 'N':
      dash();dot();break;
    case 'O':
      dash();dash();dash();break;
    case 'P':
      dot();dash();dash();dot();break;
    case 'Q':
      dash();dash();dot();dash();break;
    case 'R':
      dot();dash();dot();break;
    case 'S':
      dot();dot();dot();break;
    case 'T':
      dash();break;
    case 'U':
      dot();dot();dash();break;
    case 'V':
      dot();dot();dot();dash();break;
    case 'W':
      dot();dash();dash();break;
    case 'X':
      dash();dot();dot();dash();break;
    case 'Y':
      dash();dot();dash();dash();break;
    case 'Z':
      dash();dash();dot();dot();break;
    case ' ':
      delay(tdot*4);
      break;
    case '.':
      dot();dash();dot();dash();dot();dash();break;
    case ',':
      dash();dash();dot();dot();dash();dash();break;
    case ':':
      dash();dash();dash();dot();dot();break;
    case '?':
      dot();dot();dash();dash();dot();dot();break;
    case '\'':
      dot();dash();dash();dash();dash();dot();break;
    case '-':
      dash();dot();dot();dot();dot();dash();break;
    case '/':
      dash();dot();dot();dash();dot();break;
    case '(':
    case ')':
      dash();dot();dash();dash();dot();dash();break;
    case '\"':
      dot();dash();dot();dot();dash();dot();break;
    case '@':
      dot();dash();dash();dot();dash();dot();break;
    case '=':
      dash();dot();dot();dot();dash();break;
    case '0':
     dash();dash();dash();dash();dash();break;
    case '1':
     dot();dash();dash();dash();dash();break;
    case '2':
     dot();dot();dash();dash();dash();break;
    case '3':
     dot();dot();dot();dash();dash();break;
    case '4':
     dot();dot();dot();dot();dash();break;
    case '5':
     dot();dot();dot();dot();dot();break;
    case '6':
     dash();dot();dot();dot();dot();break;
    case '7':
     dash();dash();dot();dot();dot();break;
    case '8':
     dash();dash();dash();dot();dot();break;
    case '9':
     dash();dash();dash();dash();dot();break;

    }
    Serial.print(str.charAt(i));
    delay(3*tdot); //espace entre chaque lettre
  }
}
